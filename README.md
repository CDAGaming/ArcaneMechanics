<h1 align="center">Arcane Mechanics - REBORN (1.12.2)</h1>

<h2>About</h2>
Do you want to be a Wizard, or do you want to be a Gearhead?
<p></p>
This modpack combines both magic with technology in a one of a kind, huge modpack.
<p></p>
Explore the Galaxy, meet new friends and foes, or you know...show of the cool new items you have.

<h2>Mod List // Original Repos</h2>
- [Actually Additions](https://github.com/Ellpeck/ActuallyAdditions/tree/master)
- [Advanced Rocketry](https://github.com/zmaster587/AdvancedRocketry/tree/1.12)
- [Aether Legacy](https://github.com/Modding-Legacy/Aether-Legacy/tree/1.12.2)
- [Animania](https://minecraft.curseforge.com/projects/animania)
- [Chisel](https://github.com/Chisel-Team/Chisel/tree/1.12/dev)
- [ComputerCraft](https://github.com/dan200/ComputerCraft/tree/master)
- [DecoCraft 2](https://minecraft.curseforge.com/projects/decocraft2)
- [Extra Planets](https://github.com/MJRLegends/ExtraPlanets/tree/dev_1.12.2)
- [Galacticraft](https://github.com/micdoodle8/Galacticraft/tree/MC1.12)
- [HWYLA](https://github.com/TehNut/HWYLA/tree/1.12)
- [In-Game-Wiki](https://github.com/MineMaarten/IGW-mod/tree/master)
- [Inventory Tweaks](https://github.com/Inventory-Tweaks/inventory-tweaks/tree/develop)
- [JustEnoughItems](https://github.com/mezz/JustEnoughItems/tree/1.12)
- [JourneyMap](https://minecraft.curseforge.com/projects/journeymap)
- [Mo' Creatures](http://www.mocreatures.org/downloads)
- [More Planets](https://github.com/SteveKunG/MorePlanets/tree/1.12.2)
- [Mr CrayFish Furniture Mod](https://github.com/MrCrayfish/MrCrayfishFurnitureMod/tree/master)
- [Optifine](https://optifine.net/home)
- [Pam's HarvestCraft](https://github.com/MatrexsVigil/harvestcraft/tree/master)
- [PneumatiCraft: Repressurized](https://github.com/TeamPneumatic/pnc-repressurized/tree/master)
- [TabbyChat](https://github.com/killjoy1221/TabbyChat-2/tree/master)
- [VeinMiner](https://github.com/portablejim/VeinMiner/tree/1.12)
- [WAWLA](https://github.com/Darkhax-Minecraft/WAWLA/tree/master)
